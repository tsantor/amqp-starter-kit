# AMQP Starter Kit
Tim Santor <tsantor@xstudios.com>

## Overview
A quick head start on creating a AMQP subscriber and/or publisher client.

## Install
For this example, we're leveraging [RabbitMQ](https://www.rabbitmq.com/) and [Pika](https://pika.readthedocs.io/).

### Mac OS X
```bash
$ brew install rabbitmq
$ brew services start rabbitmq
```

### Ubuntu/Debian
```bash
$ sudo apt-get install rabbitmq-server
```

### Virtualenv
```bash
mkvirtualenv amqp_env
pip install -r requirements.txt
```

## Running the Publisher/Subscriber
Open a terminal window and run:
```bash
$ python subscriber.py --config=client.cfg
# In another terminal window
$ python publisher.py --config=publisher.cfg
```

> **NOTE:** You can open terminal windows and run as many clients as you want. Ensure you create a config for each client and give each a unique `id`.
