import logging
import os
import signal
import sys

import pika

from client import config

logger = logging.getLogger(__name__)

# Stop annoying pika logging
# logging.getLogger('pika').setLevel(logging.INFO)
logging.getLogger('pika').propagate = False

# -----------------------------------------------------------------------------

connection = None


def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    # logger.info('signal -%d received', signum)
    logger.info('Received graceful shutdown request')
    connection.close()


def main():
    """Run script."""
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    # signal.signal(signal.SIGINT, _handle_shutdown_signals)
    # signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get('default', 'host')
    # port = config.getint('default', 'port')
    vhost = config.get('default', 'vhost')
    user = config.get('default', 'user')
    password = config.get('default', 'password')
    socket_timeout = config.getint('default', 'socket_timeout')

    # Access AMQP_URL (fallback to localhost)
    url = os.environ.get('AMQP_URL', 'amqp://{user}:{password}@{host}/{vhost}'.format(
        user=user, password=password, host=host, vhost=vhost
    ))
    params = pika.URLParameters(url)
    params.socket_timeout = socket_timeout

    global connection
    connection = pika.BlockingConnection(params)  # Connect to AMQP
    channel = connection.channel()  # start a channel
    channel.exchange_declare(exchange='example', exchange_type='fanout')

    # send a message
    message = "Hello World!"
    channel.basic_publish(exchange='example', routing_key='', body=message,)
    print('[x] Sent "%r" to consumer' % message)
    connection.close()

    logger.info('Publisher shut down gracefully')
    # sys.exit()


if __name__ == '__main__':
    main()
