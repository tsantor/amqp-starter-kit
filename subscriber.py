import logging
import os
import signal
import sys
import time

import pika

from client import config

logger = logging.getLogger(__name__)

# Stop annoying pika logging
# logging.getLogger('pika').setLevel(logging.INFO)
logging.getLogger('pika').propagate = False

# -----------------------------------------------------------------------------

connection = None


def _handle_shutdown_signals(signum, frame):
    assert signum in (signal.SIGINT, signal.SIGTERM)
    # logger.info('signal -%d received', signum)
    logger.info('Received graceful shutdown request')
    connection.close()


def main():
    """Run script."""
    # Things seem to fail more gracefully if we trigger the stop
    # out of band (with a signal handler) instead of catching the
    # KeyboardInterrupt...
    # signal.signal(signal.SIGINT, _handle_shutdown_signals)
    # signal.signal(signal.SIGTERM, _handle_shutdown_signals)

    # Get config items
    host = config.get('default', 'host')
    # port = config.getint('default', 'port')
    vhost = config.get('default', 'vhost')
    user = config.get('default', 'user')
    password = config.get('default', 'password')
    socket_timeout = config.getint('default', 'socket_timeout')

    # Access AMQP_URL (fallback to localhost)
    url = os.environ.get('AMQP_URL', 'amqp://{user}:{password}@{host}/{vhost}'.format(
        user=user, password=password, host=host, vhost=vhost
    ))
    params = pika.URLParameters(url)
    params.socket_timeout = socket_timeout

    global connection
    connection = pika.BlockingConnection(params)  # Connect to AMQP
    channel = connection.channel()  # start a channel

    channel.exchange_declare(exchange='example', exchange_type='fanout')

    result = channel.queue_declare(exclusive=True)
    queue_name = result.method.queue

    channel.queue_bind(exchange='example', queue=queue_name)

    # create a function which is called on incoming messages
    def callback(channel, method, properties, body):
        print("[x] Received %r" % body)
        print("Simulate processing...")
        time.sleep(2)
        print("Simulated processing finished")

    # set up subscription on the queue
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(callback, queue=queue_name, no_ack=True)

    print('[*] Waiting for messages. To exit press CTRL+C')

    # start consuming (blocks)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    connection.close()

    logger.info('Client shut down gracefully')
    # sys.exit()


if __name__ == '__main__':
    main()
