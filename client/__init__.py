# -*- coding: utf-8 -*-

import argparse
import logging
import logging.handlers
import os
import sys

# 3rd Party
import six.moves.configparser as configparser

# -----------------------------------------------------------------------------

# Parse command line arguments
parser = argparse.ArgumentParser(description='Run a AMQP client.')
parser.add_argument('--config', help='Config file path',
                    default='client.cfg')
args = parser.parse_args()

# Read config
config_file = os.path.expanduser(args.config)
config = configparser.ConfigParser()
config.optionxform = str  # Pass config options case sensitively
config.read(config_file)

# Setup logging
logger = logging.getLogger()
logger.setLevel(config.get('default', 'log_level'))

# Create logging format
msg_fmt = '[%(levelname)s] [%(asctime)s] [%(name)s] %(message)s'
date_fmt = '%Y-%m-%d %I:%M:%S %p'
formatter = logging.Formatter(msg_fmt, date_fmt)

# Create file handler
logfile = os.path.expanduser(config.get('default', 'log'))
if not os.path.exists(os.path.dirname(logfile)):
    os.makedirs(os.path.dirname(logfile))
fh = logging.handlers.RotatingFileHandler(logfile, maxBytes=10485760, backupCount=5)
# fh = logging.handlers.TimedRotatingFileHandler(logfile, when='d', interval=1, backupCount=7)
fh.setLevel(config.get('default', 'log_level'))
fh.setFormatter(formatter)

# Create handler
ch = logging.StreamHandler()
ch.setLevel(config.get('default', 'log_level'))
ch.setFormatter(formatter)

# Add logging handlers
logger.addHandler(fh)
logger.addHandler(ch)
